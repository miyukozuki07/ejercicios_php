<!DOCTYPE html>
<html lang="en">
<head>
    <title>Formulario</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre.min.css">
    <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre-exp.min.css">
    <link rel="stylesheet"
          href="https://unpkg.com/spectre.css/dist/spectre-icons.min.css">
</head>

<body>
<div class="container">
    <div class= "columns">
        <form action="login.php" method="POST">

        <label class="form-label" for="input-text">Número de cuenta:</label>
            <input name="No_cuenta" class="form-input " type="text" id="input-noCuenta" placeholder="No. cuenta">
            <br>

        <label class="form-label" for="input-password">Contraseña:</label>
			<input name="contrasena" class="form-input" type="password" id="input-password" placeholder="Contraseña">
        <br>
        <!-- Botones -->
		<input type='submit' class="btn" value="Entrar"/>
		<input type='reset' class="btn btn-primary" value="limpiar"/>
    </div>
</div>
    
</body>
    <?php session_start();
        
        $_SESSION['Alumno'] = [
            1 => [
                'No_cuenta' => '1',
                'nombre' => 'Admin',
                'primer_apellido' => 'General',
                'segundo_apellido' => '',
                'contrasena' => 'adminpass123.',
                'genero' => 'O',
                'fecha_nac' => '25/01/1990'
            ],
        ];

        $_SESSION['autenticarUsuario'] = "";
        $bandera = 0;

        foreach($_SESSION['Alumno'] as $llave => $valor){
            if(!empty($_POST)){
                if($valor['No_cuenta'] == $_POST['No_cuenta']){
                    $bandera++;
                    $_SESSION['autenticarUsuario'] = $_POST['No_cuenta'];
                    if($valor['contrasena'] == $_POST['contrasena']){
                        $bandera++;
                    }
                }
            }
        }
       
        if($bandera == 2){
            header('Location: info.php');
        }

	?>
</html>