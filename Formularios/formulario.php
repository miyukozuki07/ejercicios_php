<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre.min.css">
    <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre-exp.min.css">
    <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre-icons.min.css">
    <title>Registro</title>
</head>
<body>
<div class="container">
    <a href="info.php">Regresar</a>
    <div class= "columns">
        <form action="formulario.php" method="POST">

        <label class="form-label" for="input-text">Número de cuenta:</label>
            <input name="No_cuenta" class="form-input " type="text" id="input-noCuenta" placeholder="No. cuenta">
            

        <label class="form-label" for="input-text">Nombre:</label>
			<input name="nombre" class="form-input" type="text" id="input-nombre" placeholder="Nombre">
            
        
        <label class="form-label" for="input-text">Primer apellido:</label>
            <input name="primer_apellido" class="form-input" type="text" id="input-primerAp" placeholder="Primer Apellido">
            
        
        <label class="form-label" for="input-text">Segundo apellido:</label>
            <input name="segundo_apellido" class="form-input" type="text" id="input-segundoAp" placeholder="Segundo Apellido">
            
        
        <label class="form-label">Género:</label>
			<label class="form-radio">
				<input type="radio" name="genero" value="H" checked>
					<i class="form-icon"></i> Hombre
			</label>

			<label class="form-radio">
				<input type="radio" name="genero" value="M">
				    <i class="form-icon"></i> Mujer
			</label>

            <label class="form-radio">
				<input type="radio" name="genero" value="O">
				    <i class="form-icon"></i> Otro
			</label>
            
        
        <label class="form-label" for="input-text">Fecha de nacimiento:</label>
			<input name="fecha_nac" class="form-input" type="text" id="input-fechaNac" placeholder="dd/mm/aaaa">
            
        
        <label class="form-label" for="input-password">Contraseña:</label>
			<input name="contrasena" class="form-input" type="password" id="input-password" placeholder="Contraseña">
        <br>
        <!-- Botones -->
		<input type='submit' class="btn" value="Registrar"/>
		<input type='reset' class="btn btn-primary" value="limpiar"/>
    </div>
</div>


<?php
    session_start();

    //print_r ($_SESSION['Alumno']);
    

    foreach($_SESSION['Alumno'] as $llave => $valor){
        if(!empty($_POST)){
            array_push($_SESSION['Alumno'], array('No_cuenta' => $_POST['No_cuenta'],
            'nombre' => $_POST['nombre'], 
            'primer_apellido' => $_POST['primer_apellido'],
            'segundo_apellido' => $_POST['segundo_apellido'],
            'contrasena' => $_POST['contrasena'],
            'genero' =>$_POST['genero'],
            'fecha_nac' => $_POST['fecha_nac']
            )
            );
        }
    }
    
    //print_r ($_SESSION['Alumno']);
    
?>
</body>
</html>