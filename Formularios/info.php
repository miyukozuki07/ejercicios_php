<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Página Principal</title>
    <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre.min.css">
    <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre-exp.min.css">
    <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre-icons.min.css">

    <style>
        #menuOption{
            background-color: #CA1572;
            height: 40px;
            padding: 10px;
        }
        
        #OptionHome{
            color: #FFFFFF;
            padding: 6px;
        }

        #OptionRegistro{
            color: #FFFFFF;
            padding: 6px;
        }

        #OptionSalir{
            color: #FFFFFF;
            padding: 6px;
        }

        #tabla-datos-guardados{
            border-collapse: separate;
            width: 40%;
        }
        
        #tabla-datos-usuario{
            border-collapse: separate;
            width: 40%;
        }

        #datos-usuario{
            padding: 20px;
        }

        #fila-nombre{
            background-color: #CBC6C8;
        }

        #datos-guardados{
            padding: 20px;
        }


</style>


</head>
<body>

<div id="menuNav">
    <nav id="menuOption">
        <a href="info.php" id="OptionHome">Home</a>
        <a href="formulario.php" id="OptionRegistro">Registrar alumnos</a>
        <a href="login.php" id="OptionSalir">Cerrar Sesión</a>
    </nav>
</div>
    
    <?php session_start();
        echo "<div id= \"datos-usuario\">";
            echo "<br>";
            echo "<h2 id=\"encabezado-usuario\">Usuario autenticado</h2>";
            echo "<table id=\"tabla-datos-usuario\" border=1>";
            echo "<tr id=\"tabla-usuarioAutenticado\">";
        
            foreach($_SESSION['Alumno'] as $llave => $valor){
                    if($valor['No_cuenta'] == $_SESSION['autenticarUsuario']){
                    echo "<tr id=\"fila-nombre\">";
                        echo "<td>";
                        echo $valor['nombre']." ".$valor['primer_apellido'];
                        echo "<br>";
                        echo "</td>";
                    echo "</tr>";
                    echo "<tr>";
                        echo "<td>";
                        echo "<h5>Información</h5>";
                        echo "Número de cuenta: ".$_SESSION['autenticarUsuario'];
                        echo "<br>";
                        echo "Fecha de nacimiento: ".$valor['fecha_nac'];
                        echo "</td>";
                    echo "</tr>";
            }
        }
        echo "</tr>";
        echo "</table>";
        echo "</div>";

        echo "<br>";


        echo "<div id= \"datos-guardados\">";    
            echo "<h2 id=\"encabezado-datos\">Datos guardados</h2>";
            echo "<table id=\"tabla-datos-guardados\" border=1>";
            echo "<tr id=\"tabla-datos\">";
        echo "<th>#</th>";
            echo "<th>Nombre</th>";
            echo "<th>Fecha de nacimiento</th>";
        echo "</tr>";
            //echo "<br>";
            
foreach($_SESSION['Alumno'] as $llave => $valor){
                if(!empty($_SESSION)){
                    echo "<tr>";
                    echo "<td>";
                    echo "<center>$llave</center>";
                    echo "</td>";
                    echo "<td>";
                    echo $valor['nombre'];
                    echo "</td>";
                    echo "<td>";
                    echo $valor['fecha_nac'];
                    echo "</td>";
                    echo "</tr>";
                }
            }
        echo "</table>";
        echo "</div>";
    
    ?>
</body>
</html>