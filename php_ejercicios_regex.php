//Nombre: Paola de los Angeles Avila Reyes.

<?php 
//Realizar una expresión regular que detecte emails correctos.

$expresion_email = "/^([a-zA-Z0-9\.]+@+[a-zA-Z]+(\.)+[a-zA-Z]{2,3})$/";

//Realizar una expresion regular que detecte Curps Correctos
//ABCD123456EFGHIJ78.
$expresion_curp = "/^([\A-Z]{4}[0-9]{6}[HM]{1}[A-Z]{2}[BCDFGHJKLMNPQRSTVWXYZ]{3}([A-Z]{2})?([0-9]{2}))$/"


//Realizar una expresion regular que detecte palabras de longitud mayor a 50
//formadas solo por letras.
$expresion_longitud = "/^([A-Z]{50,1000}(\s+[A-Z]{50,1000})*)$/"

//Crea una funcion para escapar los simbolos especiales.
$expresion_simbolos = "/^([-[\]{}()*+?.,\\^$|#\s]/g, '\\$&')$/"

//Crear una expresion regular para detectar números decimales.
$expresion_decimales = "/(^[0-9]{1,3}$|^[0-9]{1,3}\.[0-9]{1,3}$)/"

?>